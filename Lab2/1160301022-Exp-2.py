import cv2
import numpy as np
import matplotlib.pyplot as plt
import pytest
import time
from PIL import Image



def brightness_adjust(rgb_matrix, alpha):
    yuv_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2YUV).astype(np.float)
    yuv_matrix[:, :, 0] += alpha * 255
    yuv_matrix[yuv_matrix > 255] = 255
    yuv_matrix[yuv_matrix < 0] = 0
    yuv_matrix = yuv_matrix.astype(np.uint8)
    ans = cv2.cvtColor(yuv_matrix, cv2.COLOR_YUV2RGB)
    return ans.astype(np.uint8)


def contrast_adjust(rgb_matrix, alpha):
    yuv_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2YUV).astype(np.float)
    yuv_matrix[:, :, 0] = yuv_matrix[:, :, 0] ** alpha
    yuv_matrix[yuv_matrix > 255] = 255
    yuv_matrix = yuv_matrix.astype(np.uint8)
    ans = cv2.cvtColor(yuv_matrix, cv2.COLOR_YUV2RGB)
    return ans


def saturation_adjust(rgb_matrix, alpha):
    hsv_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2HSV).astype(np.float)
    hsv_matrix[:, :, 1] *= alpha
    hsv_matrix[hsv_matrix > 255] = 255
    hsv_matrix = hsv_matrix.astype(np.uint8)
    ans = cv2.cvtColor(hsv_matrix, cv2.COLOR_HSV2RGB)
    return ans


def hue_adjust(rgb_matrix, alpha):
    hsv_matrix = cv2.cvtColor(rgb_matrix, cv2.COLOR_RGB2HSV).astype(np.float)
    hsv_matrix[:, :, 0] *= alpha
    hsv_matrix[hsv_matrix > 255] = 255
    hsv_matrix = hsv_matrix.astype(np.uint8)
    ans = cv2.cvtColor(hsv_matrix, cv2.COLOR_HSV2RGB)
    return ans


def show_histogram(rgb_matrix):
    transform_matrix = np.array([0.299, 0.587, 0.114])
    gray_matrix = rgb_matrix.astype(np.float).dot(transform_matrix)
    gray_matrix = gray_matrix.astype(np.uint8)
    hist = np.bincount(gray_matrix.flatten(), minlength=256)
    plt.bar(range(256), hist, width=1)
    plt.show()


def median_filtering(rgb_matrix, filter_size):
    mid = (filter_size ** 2 + 1) // 2

    def color_filtering(color_matrix):
        h, w = color_matrix.shape
        padding_size = (filter_size - 1) // 2
        padding_color_matrix = np.pad(color_matrix,
            ((padding_size, padding_size), (padding_size, padding_size)), 'edge')
        ans = np.zeros((h, w))
        for i in range(padding_size, h + padding_size):
            for j in range(padding_size, w + padding_size):
                flatten = padding_color_matrix[i-padding_size:i+padding_size+1, j-padding_size:j+padding_size+1].flatten()
                mdn = np.int(np.median(flatten))
                ans[i - padding_size, j - padding_size] = mdn
        ans = ans.astype(np.uint8)
        return ans
    processed = np.zeros(rgb_matrix.shape, dtype=np.uint8)
    for idx in range(3):
        processed[:, :, idx] = color_filtering(rgb_matrix[:, :, idx])
    return processed


def fast_median_filtering(rgb_matrix, filter_size):
    thresh = (filter_size ** 2 - 1) // 2
    mid = (filter_size ** 2 + 1) // 2

    def fix_median(histogram, threshold, num_init, now_mdn):
        num = num_init
        if num_init <= threshold:
            for i in range(now_mdn, 256):
                num += histogram[i]
                if num > threshold:
                    return i, num - histogram[i]
        elif num_init > threshold:
            for i in range(now_mdn)[::-1]:
                num -= histogram[i]
                if num <= threshold:
                    return i, num
        return -1, 256

    def color_filtering(color_matrix):
        h, w = color_matrix.shape
        padding_size = np.int((filter_size - 1) / 2)
        padding_color_matrix = np.pad(color_matrix,
            ((padding_size, padding_size), (padding_size, padding_size)), 'edge')
        ans = np.zeros((h, w))
        for i in range(padding_size, h + padding_size):
            flatten = padding_color_matrix[i-padding_size:i+padding_size+1, 0:filter_size].flatten()
            hist = np.bincount(flatten, minlength=256)
            mdn = np.sort(flatten)[mid]
            Ltmdn = np.sum(flatten < mdn)
            ans[i - padding_size, 0] = mdn
            for j in range(padding_size+1, w+padding_size):
                for k in range(i-padding_size, i+padding_size+1):
                    left = padding_color_matrix[k, j-padding_size-1]
                    right = padding_color_matrix[k, j+padding_size]
                    if left < mdn:
                        Ltmdn -= 1
                    if right < mdn:
                        Ltmdn += 1
                    hist[left] -= 1
                    hist[right] += 1
                mdn, Ltmdn = fix_median(hist, thresh, Ltmdn, mdn)
                ans[i - padding_size, j - padding_size] = mdn
        ans = ans.astype(np.uint8)
        return ans
    processed = np.zeros(rgb_matrix.shape, dtype=np.uint8)
    for idx in range(3):
        processed[:, :, idx] = color_filtering(rgb_matrix[:, :, idx])
    return processed


def mean_filtering(rgb_matrix, filter_size):
    def color_filtering(color_matrix):
        h, w = color_matrix.shape
        padding_size = (filter_size - 1) // 2
        padding_color_matrix = np.pad(color_matrix,
            ((padding_size, padding_size), (padding_size, padding_size)), 'edge')
        ans = np.zeros((h, w))
        for i in range(padding_size, h + padding_size):
            for j in range(padding_size, w + padding_size):
                flatten = padding_color_matrix[i-padding_size:i+padding_size+1, j-padding_size:j+padding_size+1].flatten()
                mean = (np.sum(flatten) - padding_color_matrix[i, j]) / (filter_size ** 2 - 1)
                ans[i - padding_size, j - padding_size] = mean
        ans = ans.astype(np.uint8)
        return ans
    processed = np.zeros(rgb_matrix.shape, dtype=np.uint8)
    for idx in range(3):
        processed[:, :, idx] = color_filtering(rgb_matrix[:, :, idx])
    return processed


def filtering(rgb_matrix, input_filter):
    transform_matrix = np.array([0.299, 0.587, 0.114])
    gray_matrix = rgb_matrix.dot(transform_matrix)
    h, w = gray_matrix.shape
    kernel_size = input_filter.shape[0]
    if kernel_size % 2 == 1:
        padding_size = (kernel_size - 1) // 2
        padding_color_matrix = np.pad(gray_matrix,
                                      ((padding_size, padding_size), (padding_size, padding_size)), 'constant')
        ans = np.zeros(gray_matrix.shape)
        for i in range(padding_size, h + padding_size):
            for j in range(padding_size, w + padding_size):
                ans[i - padding_size, j - padding_size] = \
                    np.sum(padding_color_matrix[i-padding_size:i+padding_size+1, j-padding_size:j+padding_size+1].flatten()
                           * input_filter.flatten())
    else:
        padding_size = kernel_size // 2
        padding_color_matrix = np.pad(gray_matrix,
                                      ((padding_size, padding_size), (padding_size, padding_size)), 'constant')
        ans = np.zeros(gray_matrix.shape)
        for i in range(padding_size, h + padding_size):
            for j in range(padding_size, w + padding_size):
                ans[i - padding_size, j - padding_size] = \
                    np.sum(padding_color_matrix[i - padding_size:i + padding_size,
                           j - padding_size:j + padding_size].flatten()
                           * input_filter.flatten())
    plt.imshow(ans, cmap='gray')
    plt.show()
    return ans


def sobel(rgb_matrix, thresh):
    sobel_matrix = np.array([[1, 0, -1],
                             [2, 0, -1],
                             [1, 0, -1]])
    x_matrix = filtering(rgb_matrix, sobel_matrix)
    y_matrix = filtering(rgb_matrix, sobel_matrix.T)
    mag = np.sqrt(x_matrix ** 2 + y_matrix ** 2)
    output_image = mag.copy()
    output_image[mag < thresh] = 0
    return output_image


def roberts(rgb_matrix):
    roberts_matrix = np.array([[1, 0],
                               [0, -1]])
    x_matrix = filtering(rgb_matrix, roberts_matrix)
    y_matrix = filtering(rgb_matrix, roberts_matrix[:, ::-1])
    mag = np.sqrt(x_matrix ** 2 + y_matrix ** 2)
    thresh = 70
    output_image = mag.copy()
    output_image[mag < thresh] = 0
    return output_image


@pytest.mark.brightness
def test_brightness():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    cnt = 1
    for i in np.arange(-0.4, 0.5, 0.1):
        after = brightness_adjust(rgb_matrix, i)
        plt.subplot(3, 3, cnt)
        plt.imshow(after)
        cnt += 1
    plt.show()


@pytest.mark.contrast
def test_contrast():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    cnt = 1
    for i in np.arange(0, 1.3, 0.15):
        after = contrast_adjust(rgb_matrix, i)
        plt.subplot(3, 3, cnt)
        plt.imshow(after)
        cnt += 1
    plt.show()


@pytest.mark.saturation
def test_saturation():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    cnt = 1
    for i in np.arange(0, 4.5, 0.5):
        after = saturation_adjust(rgb_matrix, i)
        plt.subplot(3, 3, cnt)
        plt.imshow(after)
        cnt += 1
    plt.show()


@pytest.mark.hue
def test_hue():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    cnt = 1
    for i in np.arange(0, 1.3, 0.15):
        after = hue_adjust(rgb_matrix, i)
        plt.subplot(3, 3, cnt)
        plt.imshow(after)
        cnt += 1
    plt.show()


@pytest.mark.histogram
def test_histogram():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    show_histogram(rgb_matrix)
    rgb_matrix = cv2.imread('test_2.jpg')[:, :, ::-1]
    show_histogram(rgb_matrix)


@pytest.mark.median
def test_median():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    after = median_filtering(rgb_matrix, 3)
    plt.imshow(after)
    plt.show()
    after = fast_median_filtering(rgb_matrix, 3)
    plt.imshow(after)
    plt.show()


@pytest.mark.mean
def test_mean():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    after = mean_filtering(rgb_matrix, 3)
    plt.imshow(after)
    plt.show()


@pytest.mark.Sobel
def test_sobel():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    after = sobel(rgb_matrix, 70)
    plt.imshow(after, cmap='gray')
    plt.show()


@pytest.mark.Roberts
def test_roberts():
    rgb_matrix = cv2.imread('test_1.jpg')[:, :, ::-1]
    after = roberts(rgb_matrix)
    plt.imshow(after, cmap='gray')
    plt.show()

import cv2
import numpy as np
from matplotlib import pyplot as plt
import Lab3.watermark_remove as image_completion
import Lab3.utils as utils


def remove_watermark(file_name, x, y, w, h):
    rgb = cv2.imread(file_name)
    mask = np.zeros(rgb.shape[:-1])
    mask[y:y+h, x:x+w] = 1
    yx_indices, features = image_completion.generate_features(rgb, mask)
    kdtree = image_completion.KDTree(yx_indices, features)
    kdtree.gen_offset(7)
    peaks = kdtree.offsets_statistics()
    energy = utils.Energy(rgb, mask, peaks)
    cur_labels = energy.generate_labels()
    cur_labels = energy.optimize(cur_labels)
    completed_image = image_completion.complete_image(rgb, energy.mask_points, peaks, cur_labels)
    plt.imshow(completed_image[:, :, ::-1])
    plt.show()


def remove_background(file_name, mask_name):
    graph_cut = utils.GraphCut()
    graph_cut.load(file_name, mask_name)
    graph_cut.process()
    graph_cut.show_image()


# remove_background('3-1.jpg', '蒙版.jpg')
remove_watermark('3-1.jpg', 340, 840, 340, 30)



import cv2
import matplotlib.pyplot as plt
import numpy as np
import Lab3.config as config
import maxflow
from itertools import combinations


class GraphCut:
    def __init__(self):
        self.image = None
        self.mask_image = None
        self.mask = None
        self.points_image = None
        self.background_points = []
        self.foreground_points = []
        self.background_mean = np.zeros(3)
        self.foreground_mean = np.zeros(3)
        self.ground_nodes = []
        self.image_nodes = []
        self.result = None

    def load(self, image_filename, mask_filename):
        self.image = cv2.imread(image_filename)
        self.mask_image = cv2.imread(mask_filename)
        plt.imshow(self.image[:, :, ::-1])
        plt.show()
        plt.imshow(self.mask_image[:, :, ::-1])
        plt.show()
        rgb_sum = self.mask_image.sum(axis=2)
        self.mask_image[rgb_sum > 300, :] = 0
        self.result = np.zeros((self.image.shape[0], self.image.shape[1]))
        self.points_image = self.mask_image.copy()
        height, width = self.mask_image.shape[:-1]
        for x in np.arange(width, dtype=np.int):
            for y in np.arange(height, dtype=np.int):
                if 253 <= self.mask_image[y, x, 2] <= 255:
                    if not (x, y) in self.background_points:
                        self.background_points.append((x, y))
                elif 253 <= self.mask_image[y, x, 1] <= 255:
                    if not (x, y) in self.foreground_points:
                        self.foreground_points.append((x, y))

    def process(self):
        assert len(self.background_points) != 0 and len(self.foreground_points) != 0
        self.fill_result()
        self.generate_graph()
        self.cut_graph()

    def fill_result(self):
        self.result.fill(0.5)
        for (x, y) in self.background_points:
            self.result[y, x] = config.BACKGROUND
        for (x, y) in self.foreground_points:
            self.result[y, x] = config.FOREGROUND

    def generate_graph(self):
        gray_image = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        G = self.mask_image[:, :, 1]
        R = self.mask_image[:, :, 2]
        obj_num = np.sum(G == 255)
        obj_hist = np.bincount(gray_image[G == 255], minlength=256)
        back_num = np.sum(R == 255)
        back_hist = np.bincount(gray_image[R == 255], minlength=256)
        height, width = self.image.shape[:-1]

        def cal_R(cur_y, cur_x, ground_type):
            if ground_type == config.BACKGROUND:
                if obj_hist[gray_image[cur_y, cur_x]] == 0:
                    return 10
                else:
                    return - np.log(obj_hist[gray_image[cur_y, cur_x]] / obj_num)
            elif ground_type == config.FOREGROUND:
                if back_hist[gray_image[cur_y, cur_x]] == 0:
                    return 10
                else:
                    return - np.log(back_hist[gray_image[cur_y, cur_x]] / back_num)

        for (y, x), val in np.ndenumerate(self.result):
            if val == config.BACKGROUND:
                self.ground_nodes.append((y * width + x, config.MAXIMUM, 0))
            elif val == config.FOREGROUND:
                self.ground_nodes.append((y * width + x, 0, config.MAXIMUM))
            else:
                # self.nodes.append((y * width + x, 1e-5 * cal_R(y, x, BACKGROUND), 1e-5 * cal_R(y, x, FOREGROUND)))
                self.ground_nodes.append((y * width + x, 0, 0))
        gray_image = gray_image.astype(np.float)
        for (y, x), val in np.ndenumerate(self.result):
            if y == self.image.shape[0] - 1 or x == self.image.shape[1] - 1:
                continue
            cur_idx = y * width + x
            neighbor = y * width + x + 1
            weight = np.exp(- 0.003 * (np.float(gray_image[y, x]) - np.float(gray_image[y, x+1])) ** 2)
            self.image_nodes.append((cur_idx, neighbor, weight))

            neighbor = (y + 1) * width + x
            weight = np.exp(- 0.003 * (np.float(gray_image[y, x]) - np.float(gray_image[y+1, x])) ** 2)
            self.image_nodes.append((cur_idx, neighbor, weight))

    def cut_graph(self):
        self.mask = np.zeros_like(self.image, dtype=np.bool)
        g = maxflow.Graph[float](len(self.ground_nodes), len(self.ground_nodes))
        nodes = g.add_nodes(len(self.ground_nodes))
        for ground_nodes in self.ground_nodes:
            g.add_tedge(nodes[ground_nodes[0]], ground_nodes[1], ground_nodes[2])
        for image_node in self.image_nodes:
            g.add_edge(image_node[0], image_node[1], image_node[2], image_node[2])
        flow = g.maxflow()
        for index in range(len(self.ground_nodes)):
            if g.get_segment(index) == 1:
                x, y = int(index % self.image.shape[1]), int(index / self.image.shape[1])
                self.mask[y, x] = (1, 1, 1)

    def show_image(self):
        to_show = self.image.copy()
        to_show[self.mask != 1] = 255
        plt.imshow(to_show[:, :, ::-1])
        plt.show()


def interpolation(rgb_matrix, x, y, w, h):
    ans = rgb_matrix.copy()
    height, width, channels = rgb_matrix.shape

    def bilinear_interpolation(x1, y1, x2, y2, cx, cy):
        float_matrix = rgb_matrix.astype(np.float)
        if cx != x1 and cx != x2 and cy != y1 and cy != y2:
            f_hat = float_matrix[y1, x1] * (x2 - cx) * (y2 - cy) + \
                float_matrix[y1, x2] * (cx - x1) * (y2 - cy) + \
                float_matrix[y2, x1] * (x2 - cx) * (cy - y1) + \
                float_matrix[y2, x2] * (cx - x1) * (cy - y1)
            f_hat /= (x2 - x1) * (y2 - y1)
        elif (cx == x1 or cx == x2) and (cx == y1 or cy == y2):
            f_hat = float_matrix[cy, cx]
        elif cx == x1 or x1 == x2:
            f_hat = float_matrix[y1, x1] * (y2 - cy) / (y2 - y1) + \
                float_matrix[y2, x1] * (cy - y1) / (y2 - y1)
        elif cx == x2 or x1 == x2:
            f_hat = float_matrix[y1, x1] * (y2 - cy) / (y2 - y1) + \
                float_matrix[y2, x2] * (cy - y1) / (y2 - y1)
        elif cy == y1 or y1 == y2:
            f_hat = float_matrix[y1, x1] * (x2 - cx) / (x2 - x1) + \
                float_matrix[y1, x2] * (cx - x1) / (x2 - x1)
        elif cy == y2 or y1 == y2:
            f_hat = float_matrix[y2, x1] * (x2 - cx) / (x2 - x1) + \
                float_matrix[y2, x2] * (cx - x1) / (x2 - x1)
        f_hat[f_hat < 0] = 0
        f_hat[f_hat > 255] = 255
        return np.uint8(f_hat)

    th = np.float(min(y+h, height-1) - y)
    tw = np.float(min(x+w, width-1) - x)
    for i in range(y, min(y+h, height-1)):
        for j in range(x, min(x+w, width-1)):
            ans[i, j] = bilinear_interpolation(j-w, i-1, min(x+w, width) + 1, i, j, i)
    plt.imshow(ans)
    plt.show()


base_num = config.PATCH_SIZE // 4
S_base = np.ones((config.PATCH_SIZE, 1))
S_16_1 = S_base.copy()
S_16_2 = S_base.copy()
S_16_2[2 * base_num:] = -S_16_2[2 * base_num:]
S_16_3 = S_base.copy()
S_16_3[base_num:3 * base_num] = -S_16_3[base_num:3 * base_num]
S_16_4 = S_base.copy()
S_16_4[base_num:2 * base_num] = -S_16_4[base_num:2 * base_num]
S_16_4[3 * base_num:] = -S_16_4[3 * base_num:]
S_bases = []
S_bases.append(S_16_1[::-1] * S_16_1.T)
S_bases.append(S_16_1[::-1] * S_16_2.T)
S_bases.append(S_16_2[::-1] * S_16_1.T)
S_bases.append(S_16_1[::-1] * S_16_3.T)
S_bases.append(S_16_2[::-1] * S_16_2.T)
S_bases.append(S_16_3[::-1] * S_16_1.T)
S_bases.append(S_16_1[::-1] * S_16_4.T)
S_bases.append(S_16_2[::-1] * S_16_3.T)
S_bases.append(S_16_3[::-1] * S_16_2.T)
S_bases.append(S_16_4[::-1] * S_16_1.T)
S_bases.append(S_16_2[::-1] * S_16_4.T)
S_bases.append(S_16_3[::-1] * S_16_3.T)
S_bases.append(S_16_4[::-1] * S_16_2.T)
S_bases.append(S_16_3[::-1] * S_16_4.T)
S_bases.append(S_16_4[::-1] * S_16_3.T)
S_bases.append(S_16_4[::-1] * S_16_4.T)


def RGB_to_YCbCr(rgb_matrix):
    transform_mat = np.array(
        [[0.299, 0.587, 0.114],
         [-0.169, -0.331, 0.500],
         [0.500, -0.419, -0.081]])
    YCbCr_matrix = rgb_matrix.dot(transform_mat.T) + np.array([[[0, 128, 128]]])
    YCbCr_matrix[YCbCr_matrix > 255] = 255
    YCbCr_matrix[YCbCr_matrix < 0] = 0
    YCbCr_matrix = YCbCr_matrix.astype(np.uint8)
    return YCbCr_matrix


def patch_WHT(patch):
    assert patch is not None
    height, width, _ = patch.shape
    assert height == width and height == config.PATCH_SIZE
    Y = patch[:, :, 0].flatten()
    Cb = patch[:, :, 1].flatten()
    Cr = patch[:, :, 2].flatten()
    feature = np.zeros(24)
    for i in range(16):
        feature[i] = (S_bases[i].flatten()).dot(Y) / (config.PATCH_SIZE ** 2)
    for i in range(16, 20):
        feature[i] = (S_bases[i-16].flatten()).dot(Cb) / (config.PATCH_SIZE ** 2)
    for i in range(20, 24):
        feature[i] = (S_bases[i-20].flatten()).dot(Cr) / (config.PATCH_SIZE ** 2)
    return feature


class Energy(object):
    def __init__(self, image, mask, labels):
        self.image = image/255.0
        self.mask = mask
        self.labels = labels
        self.mask_points = [[y, x] for (y, x), val in np.ndenumerate(self.mask) if val != 0]
        self.neighbors_idx = []
        self.initialize_neighbors()

    def cal_Es(self, p1, p2, sa, sb):
        y1, x1 = p1
        y2, x2 = p2
        yoffa, xoffa = sa
        yoffb, xoffb = sb
        try:
            if self.mask[y1+yoffa, x1+xoffa] == 0 and self.mask[y1+yoffb, x1+xoffb] == 0 \
                    and self.mask[y2+yoffa, x2+xoffa] == 0 and self.mask[y2+yoffb, x2+xoffb] == 0:
                return np.linalg.norm(self.image[y1+yoffa, x1+xoffa] - self.image[y1+yoffb, x1+xoffb]) ** 2 + \
                       np.linalg.norm(self.image[y2+yoffa, x2+xoffa] - self.image[y2+yoffb, x2+xoffb]) ** 2
            return 1e6
        except:
            return 1e6

    def cal_Ed(self, p, s):
        y, x = p
        yoff, xoff = s
        try:
            if self.mask[y+yoff, x+xoff] == 0:
                return 0
            return 1e8
        except:
            return 1e8

    def initialize_neighbors(self):
        self.neighbors_idx = []
        for y, x in self.mask_points:
            tmp = [[y-1, x], [y+1, x], [y, x-1], [y, x+1]]
            ans = [self.mask_points.index(neighbor) for neighbor in tmp if neighbor in self.mask_points]
            self.neighbors_idx.append(ans)
        return self.neighbors_idx.copy()


    def generate_labels(self):
        label_idx = np.zeros(len(self.mask_points))
        for i in range(len(self.mask_points)):
            temp = np.random.randint(0, len(self.labels))
            while self.cal_Ed(self.mask_points[i], self.labels[temp]) > 1:
                temp = np.random.randint(0, len(self.labels))
            label_idx[i] = temp
        return label_idx

    def create_graph(self, ab_points_idx, alpha, beta, cur_label_idx):
        num = len(ab_points_idx)
        graph = maxflow.Graph[float](2, num)
        nodes = graph.add_nodes(num)
        for i in range(num):
            taq = self.cal_Ed(self.mask_points[ab_points_idx[i]], self.labels[alpha])
            tbq = self.cal_Ed(self.mask_points[ab_points_idx[i]], self.labels[beta])
            neighbors = self.neighbors_idx[ab_points_idx[i]]
            for neighbor_idx in neighbors:
                try:
                    neighbor_label, ab_neighbor_idx \
                        = cur_label_idx[neighbor_idx], ab_points_idx.index(neighbor_idx)
                    if ab_neighbor_idx > i and (neighbor_label == alpha or neighbor_label == beta):
                        Vpq = self.cal_Es(self.mask_points[ab_points_idx[i]], self.mask_points[neighbor_idx], self.labels[alpha], self.labels[beta])
                        graph.add_edge(nodes[ab_points_idx[i]], nodes[ab_neighbor_idx], Vpq, Vpq)
                    else:
                        Vaq = self.cal_Es(self.mask_points[ab_points_idx[i]], self.mask_points[neighbor_idx], self.labels[alpha], self.labels[neighbor_label])
                        Vbq = self.cal_Es(self.mask_points[ab_points_idx[i]], self.mask_points[neighbor_idx], self.labels[beta], self.labels[neighbor_label])
                        taq += Vaq
                        tbq += Vbq
                except:
                    pass
            graph.add_tedge(nodes[i], taq, tbq)
        return graph, nodes

    def optimize(self, labels):
        last_label_idx = np.copy(labels).astype(np.int)
        cur_label_idx = np.copy(labels).astype(np.int)
        epoch_num = 0
        while True:
            success = 0
            for alpha, beta in combinations(range(len(self.labels)), 2):
                ab_point_idx = [idx for idx in range(len(self.mask_points)) if
                                (last_label_idx[idx] == alpha or last_label_idx[idx] == beta)]
                if len(ab_point_idx) > 0:
                    graph, nodes = self.create_graph(ab_point_idx, alpha, beta, last_label_idx)
                    flow = graph.maxflow()
                    for idx in range(len(ab_point_idx)):
                        seg = graph.get_segment(nodes[idx])
                        cur_label_idx[ab_point_idx[idx]] = alpha * (1 - seg) + beta * seg
                    if self.is_succeed(last_label_idx, cur_label_idx):
                        last_label_idx = cur_label_idx.copy()
                        success = 1
                    else:
                        cur_label_idx = last_label_idx.copy()
            epoch_num += 1
            print("EPOCH: ", epoch_num)
            if epoch_num >= config.EPOCH or success != 1:
                break
        return cur_label_idx





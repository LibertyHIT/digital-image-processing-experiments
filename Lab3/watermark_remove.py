import numpy as np
from scipy.spatial import cKDTree
from scipy.ndimage import gaussian_filter
from Lab3.utils import RGB_to_YCbCr, patch_WHT, Energy
import Lab3.config as config


def generate_features(image, mask):
    indices = []
    pre_YCbCr = RGB_to_YCbCr(image)
    h, w, _ = pre_YCbCr.shape
    patch_row_num = h // config.PATCH_SIZE
    patch_rows = patch_row_num * config.PATCH_SIZE
    patch_col_num = w // config.PATCH_SIZE
    patch_cols = patch_col_num * config.PATCH_SIZE
    YCbCr = pre_YCbCr[:patch_rows, :patch_cols]
    feature_matrix = []
    for i in range(patch_row_num * patch_col_num):
        row_idx = i // patch_col_num
        col_idx = i % patch_row_num
        try:
            if mask[row_idx*config.PATCH_SIZE, col_idx*config.PATCH_SIZE] != 0 or \
                    mask[(row_idx+1)*config.PATCH_SIZE, col_idx*config.PATCH_SIZE] != 0 or \
                    mask[row_idx*config.PATCH_SIZE, (col_idx+1)*config.PATCH_SIZE] != 0 or \
                    mask[(row_idx+1)*config.PATCH_SIZE, (col_idx+1)*config.PATCH_SIZE] != 0:
                continue
            indices.append([row_idx * config.PATCH_SIZE + config.PATCH_SIZE // 2,
                            col_idx * config.PATCH_SIZE + config.PATCH_SIZE // 2])
            patch = YCbCr[row_idx * config.PATCH_SIZE: (row_idx + 1) * config.PATCH_SIZE,
                    col_idx * config.PATCH_SIZE: (col_idx + 1) * config.PATCH_SIZE]
            feature_matrix.append(patch_WHT(patch).tolist())
        except:
            pass
    return np.asarray(indices, dtype=np.int), np.asarray(feature_matrix)


class KDTree:
    def __init__(self, indices, feature_matrix, tau=340/15.0):
        self.indices = indices
        self.feature_matrix = feature_matrix
        self.Tree = cKDTree(feature_matrix)
        self.tau = tau
        self.offsets = None
        self.offsets_hist = None
        self.offsets_hist_image = None

    def gen_offset(self, k):
        size = self.feature_matrix.shape[0]
        self.offsets = np.zeros((size, 2), dtype=np.int)
        for i in range(size):
            dists, indexs = self.Tree.query(self.feature_matrix[i], k=k)
            for j in range(k):
                if np.linalg.norm(self.indices[indexs[j]] - self.indices[i]) > self.tau:
                    self.offsets[i] = self.indices[indexs[j]] - self.indices[i]
                    break
        return self.offsets.copy()

    def offsets_statistics(self):
        if self.offsets is None:
            print("Please generate offsets")
            return
        min_y = np.min(self.offsets[:, 0])
        max_y = np.max(self.offsets[:, 0])
        min_x = np.min(self.offsets[:, 1])
        max_x = np.max(self.offsets[:, 1])
        self.offsets_hist_image = np.zeros((max_y - min_y + 1, max_x - min_x + 1), dtype=np.int)
        self.offsets_hist = {}
        for y, x in self.offsets:
            if (y, x) in self.offsets_hist.keys():
                self.offsets_hist[(y, x)] += 1
            else:
                self.offsets_hist[(y, x)] = 1
        for y, x in self.offsets_hist.keys():
            self.offsets_hist_image[y - min_y, x - min_x] = self.offsets_hist[(y, x)]
        self.offsets_hist_image = gaussian_filter(self.offsets_hist_image, np.sqrt(2))
        self.offsets_hist = {}
        for (y, x), val in np.ndenumerate(self.offsets_hist_image):
            self.offsets_hist[(y + min_y, x + min_x)] = val
        self.offsets_hist = sorted(self.offsets_hist.items(), key=lambda item: item[1], reverse=True)
        self.offsets_hist = np.array([offset for offset, _ in self.offsets_hist[:config.PEAK_NUM]])
        return self.offsets_hist



def complete_image(image, mask_points, offsets, cur_labels):
    result = image.copy()
    for i in range(len(mask_points)):
        j = cur_labels[i]
        try:
            result[mask_points[i][0], mask_points[i][1]] = \
                image[mask_points[i][0] + offsets[j][0], mask_points[i][1] + offsets[j][1]]
        except:
            pass
    return result


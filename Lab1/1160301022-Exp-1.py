
# coding: utf-8


import numpy as np
from matplotlib import pyplot as plt
import struct
import sys
import imageio
import cv2


def read_24bit_bmp(file_name):
    with open(file_name, 'rb') as f:
        txt = str.encode('')
        data = []
        for line in f.readlines():
            txt += line
        biWidth = struct.unpack('<i', txt[18: 22])[0]
        biHeight = struct.unpack('<i', txt[22: 26])[0]
        start = 54
        image = np.zeros((biHeight, biWidth, 3), dtype=np.uint8)
        for i in range(biHeight):
            image[biHeight - 1 - i] = np.frombuffer(txt[start: start+biWidth * 3], dtype=np.uint8).reshape(biWidth, 3)
            if biWidth * 3 % 4 == 0:
                start += biWidth * 3
            else:
                start += 4 - biWidth * 3 % 4 + biWidth * 3
    return image[:, :, ::-1]


def RGB_to_YIQ(rgb_matrix, file_name):
    transform_mat = np.array(
        [[0.299, 0.587, 0.114],
         [0.596, -0.274, -0.322],
         [0.211, -0.523, 0.312]])
    YIQ_matrix = rgb_matrix.dot(transform_mat.T)
    YIQ_matrix += np.array([[[0, 128, 128]]])
    YIQ_matrix = YIQ_matrix.astype(np.uint8)
    imageio.imsave(file_name + '-1160301022-YIQ.bmp', YIQ_matrix[:, :, ::-1])
    return YIQ_matrix


def RGB_to_HSI(rgb_matrix, file_name):
    input_data = rgb_matrix.astype(np.float) / np.sum(rgb_matrix.astype(np.float), axis=2, keepdims=True)
    R = input_data[:, :, 0].copy()
    G = input_data[:, :, 1].copy()
    B = input_data[:, :, 2].copy()
    hsi_matrix = np.zeros(input_data.shape)
    hsi_matrix[:, :, 2] = np.mean(rgb_matrix, axis=2)
    hsi_matrix[:, :, 1] = 1 - 3 * np.min(input_data, axis=2)
    hsi_matrix[:, :, 1] *= 100.0
    tmp = (R * 2 - G - B) / (2 * np.sqrt((R - G) ** 2 + (R - B) * (G - B)))
    tmp[tmp > 1] = 1
    tmp[tmp < -1] = -1
    theta = np.arccos(tmp)
    hsi_matrix[G >= B, 0] = theta[G >= B] * 180 / np.pi
    hsi_matrix[G < B, 0] = 360 - theta[G < B] * 180 / np.pi
    hsi_matrix[hsi_matrix > 255] = 255
    hsi_matrix[hsi_matrix < 0] = 0
    hsi_matrix = hsi_matrix.astype(np.uint8)
    imageio.imsave(file_name + '-1160301022-HSI.bmp', hsi_matrix[:, :, ::-1])
    return hsi_matrix


def RGB_to_YCbCr(rgb_matrix, file_name):
    transform_mat = np.array(
        [[0.299, 0.587, 0.114],
         [-0.169, -0.331, 0.500],
         [0.500, -0.419, -0.081]])
    YCbCr_matrix = rgb_matrix.dot(transform_mat.T) + np.array([[[0, 128, 128]]])
    YCbCr_matrix[YCbCr_matrix > 255] = 255
    YCbCr_matrix[YCbCr_matrix < 0] = 0
    YCbCr_matrix = YCbCr_matrix.astype(np.uint8)
    imageio.imsave(file_name + '-1160301022-YCbCr.bmp', YCbCr_matrix[:, :, ::-1])
    return YCbCr_matrix


def RGB_to_XYZ(rgb_matrix, file_name):
    transform_mat = np.array(
        [[0.412453, 0.357580, 0.180423],
         [0.212671, 0.715160, 0.072169],
         [0.019334, 0.119193, 0.950227]])
    XYZ_matrix = rgb_matrix.dot(transform_mat.T)
    XYZ_matrix[XYZ_matrix > 255] = 255
    XYZ_matrix[XYZ_matrix < 0] = 0
    XYZ_matrix = XYZ_matrix.astype(np.uint8)
    imageio.imsave(file_name + '-1160301022-XYZ.bmp', XYZ_matrix[:, :, ::-1])
    return XYZ_matrix


# 


if __name__ == '__main__':
    rgb = read_24bit_bmp(sys.argv[1])
    filename = sys.argv[1].split('.bmp')[0]
    RGB_to_YIQ(rgb, filename)
    RGB_to_HSI(rgb, filename)
    RGB_to_YCbCr(rgb, filename)
    RGB_to_XYZ(rgb, filename)

